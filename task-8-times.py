from iseseisev_projekt.helpers.read_data import read_file_as_list
import re
from collections import Counter

# Extract times
data = read_file_as_list("../iseseisev_projekt/data/ppa.txt")
times = re.findall('[0-9]{1,2}[.][0-9]{2}', data)

clock = [i for i in range(0, 24)]

hours = []
for time in times:
    hour = time.split(".")[0]
    if "0" in hour[0]:
        hour = int(hour[1])
    else:
        hour = int(hour)
    hours.append(hour)

count_hours = Counter(hours).most_common()
# Hours
for hour in count_hours:
    print(hour[0], hour[1])


