def read_file_as_list(file_path):
    with open(file_path, 'r') as src:
        return src.read()