from estnltk import Text
from nltk.corpus import stopwords
from nltk.cluster.util import cosine_distance
import numpy as np
import networkx as nx
import matplotlib.pyplot as plt

def stop_words_list(file_name):
    with open(file_name, mode = "r", encoding = "utf-8-sig") as file:
        words = file.read().split(",")
    words = [word.replace("\n", "") for word in words]
    return words

def read_article(file_name, stop_words):
    with open(file_name, mode = "r", encoding="utf-8-sig") as file:
        file_content = file.read()
        initial_sentences = Text(file_content).sentence_texts
        sentences_dict = dict()
        for sentence in initial_sentences:
            lemmas = Text(sentence).lemmas
            postags = Text(sentence).postags
            lemmas_list = [lemma.lower() for i, lemma in enumerate(lemmas) if postags[i] != 'Z' and lemma not in stop_words]
            sentences_dict[sentence] = lemmas_list
        return sentences_dict

def sentence_similarity(sent1, sent2):
    sent1 = [w.lower() for w in sent1]
    sent2 = [w.lower() for w in sent2]
    all_words = list(set(sent1 + sent2))
    vector1 = [0] * len(all_words)
    vector2 = [0] * len(all_words)
    for w in sent1:
        vector1[all_words.index(w)] += 1
    for w in sent2:
        vector2[all_words.index(w)] += 1
    return 1 - cosine_distance(vector1, vector2)

def build_similarity_matrix(sentences_dict):
    sentences = list(sentences_dict.keys())
    nr_of_sentences = len(sentences)
    similarity_matrix = np.zeros((nr_of_sentences, nr_of_sentences))
    for idx1 in range(nr_of_sentences):
        for idx2 in range(nr_of_sentences):
            if idx1 == idx2:
                continue
            similarity_matrix[idx1][idx2] = sentence_similarity(sentences[idx1], sentences[idx2])
    return similarity_matrix

def generate_summary(file_name, top_n=5):
    stop_words = stop_words_list("../iseseisev_projekt/helpers/stopwords_est.txt")
    summarize_text = []
    # Samm 1 loome lausete sõnastiku, kus võtmeteks on laused ja väärtusteks sõnade lemmad (stopp-sõnu on välditud)

    sentences_dict = read_article(file_name, stop_words)

    # Samm 2 loome sarnasusmaatriksi, milles on lausete arv ridu ja veerge ja kus sarnasus lausete vahel on leitud kasutades
    # koosinussarnasust lausete lemmade vahel
    sentence_similarity_martix = build_similarity_matrix(sentences_dict)

    # Samm 3 loome sarnasusmaatriksi alusel graafi ja leiame lausete␣ olulisusskoorid
    sentence_similarity_graph = nx.from_numpy_array(sentence_similarity_martix)
    #plt.figure(figsize=(12,6))
    #nx.draw_networkx(sentence_similarity_graph, node_color='lime')
    scores = nx.pagerank(sentence_similarity_graph)
    # Samm 4 järjestame laused skooride alusel nii, et olulisemad on eespool
    sentences = sentences_dict.keys()
    ranked_sentence = sorted(((scores[i],s) for i,s in enumerate(sentences)), reverse=True)
    # Samm 5 eraldame top-n lauset
    for i in range(top_n):
        summarize_text.append(ranked_sentence[i][1])
    # Samma 6 ühendame top-n lauset üheks tervikuks
    return ' '.join(summarize_text)