from estnltk import Text
from estnltk.vabamorf.morf import syllabify_words
import math

'''  
    Author: @Kunnar Kukk
    Requires: estnltk-1.4.1
'''

def count_syllables_in_sentence(sentence_s):
    text =[t for t in Text(sentence_s).word_texts if t.isalpha()]
    syllables = syllabify_words(text)
    syllable_count = 0
    for word_syllable in syllables:
        for s in word_syllable:
            syllable_count += int(dict(s)['quantity'])
    return syllable_count


def complex_words_in_sentence(sentence_s):
    text = [t for t in Text(sentence_s).word_texts if t.isalpha()]
    syllables = syllabify_words(text)
    count = 0
    for word_syllable in syllables:
        for s in word_syllable:
            word_syl_quantity = int(dict(s)['quantity'])
            if word_syl_quantity >= 3:
                count += int(dict(s)['quantity'])
    return count


def count_content_words_in_sentence(sentence_s):
    t = Text(sentence_s)
    pos_tags = t.postags
    content_word = [
        "nimisona",
        "maarsona",
        "tegusona",
        "omadussona"
    ]

    types_map = {
        "A": "omadussona_algvorre",
        "C": "omadussona_keskvorre",
        "D": "maarsona",
        "G": "genitiiv_atribuut",
        "H": "parisnimi",
        "I": "hyydsona",
        "J": "sidesona",
        "K": "kaassona",
        "N": "pohiarvsona",
        "O": "jargarvsona",
        "P": "asesona",
        "S": "nimisona",
        "U": "omadussona",
        "V": "tegusona",
        "X": "sona_verbi_juures_tahendus_puudub",
        "Y": "lyhend",
        "Z": "lausemark"
    }

    c = 0
    for p in pos_tags:
        pos_tag = p[0]
        if types_map[pos_tag] in content_word:
            c += 1
    return c


def count_functional_words_in_sentence(sentence_s):
    sentence_length = len(Text(sentence_s).word_texts)
    content_words = count_content_words_in_sentence(sentence_s)
    return sentence_length - content_words


def f_score(sentence_s):
    # Assume sentence is given as string "Tere mina olen siin"
    sak = [
        "nimisona",
        "omadussona",
        "kaassona"
    ]
    pvdi = [
        "asesona",
        "tegusona",
        "maarsona",
        "hyydsona"
    ]
    types_map = {
        "A": "omadussona_algvorre",
        "C": "omadussona_keskvorre",
        "D": "maarsona",
        "G": "genitiiv_atribuut",
        "H": "parisnimi",
        "I": "hyydsona",
        "J": "sidesona",
        "K": "kaassona",
        "N": "pohiarvsona",
        "O": "jargarvsona",
        "P": "asesona",
        "S": "nimisona",
        "U": "omadussona",
        "V": "tegusona",
        "X": "sona_verbi_juures_tahendus_puudub",
        "Y": "lyhend",
        "Z": "lausemark"
    }
    t = str(sentence_s)
    t = Text(t)
    pos_tags = t.postags

    sak_counter = 0
    pvdi_counter = 0
    for p in pos_tags:
        pos_tag = p[0]
        if types_map[pos_tag] in sak:
            sak_counter += 1
        if types_map[pos_tag] in pvdi:
            pvdi_counter += 1
    f_score = float((sak_counter - pvdi_counter + 100) / 2)
    return f_score


def lexden(sentence_s):
    # Lexical density of sentence
    # Sentence is given as string "Tere mina olen siin"
    fn_words = count_functional_words_in_sentence(sentence_s)
    if fn_words == 0.0:
        return 0.0
    else:
        lexden = float(
            count_content_words_in_sentence(sentence_s) / count_functional_words_in_sentence(sentence_s) * 100.0)
    return lexden


def fog(sentence_s):
    # Assume sentence is given as string "Tere mina olen siin"
    word_count = len(Text(sentence_s).word_texts)
    sentence_count = 1  # constant
    complex_words_count = complex_words_in_sentence(sentence_s)
    fog = 0.4 * ((word_count / sentence_count) + 100.0 * (complex_words_count / word_count))
    return fog


def fres(sentence_s):
    word_count = len(Text(sentence_s).word_texts)
    sentence_count = 1  # constant
    syllable_count = count_syllables_in_sentence(sentence_s)
    fres = float(206.835 - (1.015 * (word_count / sentence_count)) - (84.6 * (syllable_count / word_count)))
    return fres


def tuldava_complexity(sentence_s):
    word_count = len(Text(sentence_s).word_texts)
    # Avg word length in syllalbles
    i = (count_syllables_in_sentence(sentence_s) / word_count)
    tuldava = i * math.log(word_count)
    return tuldava