from estnltk.wordnet import wn
from estnltk import Text
import re

def all_synonyms_of_word(word):
    try:
        synsets = [[lemma.name for lemma in synset.lemmas()] for synset in wn.synsets(word)]
        synonym_set = set()
        for synset in synsets:
            synonym_set.update(synset)
        return synonym_set
    except AttributeError:
        return None

def all_hyperonyms_synonyms(word):
    try:
        hypero_synsets = [synset.hypernyms() for synset in wn.synsets(word)]
        hypero_main_lexical_units = re.findall(r"'([^']*)'", str(hypero_synsets))
        hypero_synonyms = set()
        for lexical_unit in hypero_main_lexical_units:
            synonyms = [lemma.name for lemma in wn.synset(lexical_unit).lemmas()]
            hypero_synonyms.update(synonyms)
        return hypero_synonyms
    except AttributeError:
        return None

def is_synonym(word_1, word_2):
    try:
        if Text(word_2).lemmas[0] in all_synonyms_of_word(Text(word_1).lemmas[0]):
            print("Synonymia: ", word_1, word_2)
            return True
        else:
            return False
    except TypeError:
        return False


def is_hyperonym(word_1, word_2):
    try:
        if Text(word_2).lemmas[0] in all_hyperonyms_synonyms(Text(word_1).lemmas[0]):
            print("Hyperonymia: ", word_1, word_2)
            return True
        else:
            return False
    except TypeError:
        return False