from sklearn import svm, model_selection
from iseseisev_projekt.helpers.read_data import read_file_as_list
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import accuracy_score
from estnltk import Text
import numpy as np
import pandas as pd
from collections import Counter

# https://medium.com/@bedigunjit/simple-guide-to-text-classification-nlp-using-svm-and-naive-bayes-with-python-421db3a72d34

def content_word_lemmas(sentence, content_poses):
    text = Text(sentence)
    sent_lemmas = text.lemmas
    sent_postags = text.postags
    # content_poses = ['S', 'V', 'A', 'C', 'U']
    if content_poses is not None:
        lemma_list = [sent_lemmas[i].lower() for i in range(len(sent_lemmas)) if sent_postags[i] in content_poses]
    else:
        lemma_list = [sent_lemmas[i].lower() for i in range(len(sent_lemmas))]
    if len(lemma_list) > 0:
        return lemma_list
    else:
        return None

data = read_file_as_list("../iseseisev_projekt/data/ppa.txt")
splitted_data = data.split("\n")

# Create lemmas list
lemmas = []
for item in splitted_data:
    temp_lemmas = content_word_lemmas(item, ['S', 'V', 'A', 'C', 'U'])
    lemmas.append(temp_lemmas)

# Create labels list
# Random  label
random_labels = []
for item in lemmas:
    label = np.random.randint(len(item))
    random_labels.append(label)

item_list = []
for item in lemmas:
    item_list.append(item[0])

counter = Counter(item_list).most_common()
label_list = []

# Unique labels
for item in counter:
    label_list.append(item[0])

first_word_label = []
for item in lemmas:
    first_word_label.append(label_list.index(item[0]))

# Make corpora as dataframe
df_list = []
for i in range(0, len(lemmas)):
    df_list.append([",".join(lemmas[i]), random_labels[i], first_word_label[i]])

df = pd.DataFrame(df_list, columns=['lemmatised_sentence', 'random_label', 'first_word_label'])

Tfidf_vect = TfidfVectorizer(max_features=5000)
Tfidf_vect.fit(df['lemmatised_sentence'])

Train_X, Test_X, Train_Y, Test_Y = model_selection.train_test_split(df['lemmatised_sentence'], df['first_word_label'], test_size=0.3, shuffle=False)

Encoder = LabelEncoder()
Train_Y = Encoder.fit_transform(Train_Y)
Test_Y = Encoder.fit_transform(Test_Y)

Train_X_Tfidf = Tfidf_vect.transform(Train_X)
Test_X_Tfidf = Tfidf_vect.transform(Test_X)

# Classifier - Algorithm - SVM
# fit the training dataset on the classifier
SVM = svm.SVC(C=1.0, kernel='linear', degree=2, gamma='auto')
SVM.fit(Train_X_Tfidf, Train_Y)
# predict the labels on validation dataset
predictions_SVM = SVM.predict(Test_X_Tfidf)
# Use accuracy_score function to get the accuracy
accuracy = accuracy_score(predictions_SVM, Test_Y) * 100
print("SVM Accuracy Score -> ", accuracy)

