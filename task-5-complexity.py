from iseseisev_projekt.helpers import complexity
from iseseisev_projekt.helpers.read_data import read_file_as_list
import nltk.data
import pandas as pd

def preprocess():
    data = read_file_as_list("../iseseisev_projekt/data/ppa.txt")
    data = data.replace("#ppa24h", "")
    tok = nltk.data.load('tokenizers/punkt/estonian.pickle')
    sentences = tok.tokenize(data)
    return sentences

def calc_coeficient():
    sentences = preprocess()
    target = []
    for sentence in sentences:
        tuldava = complexity.tuldava_complexity(sentence)
        fog = complexity.fog(sentence)
        fres = complexity.fres(sentence)
        lexden = complexity.lexden(sentence)
        target.append([tuldava, fog, fres, lexden])
    df = pd.DataFrame(target, columns=['tuldava', 'fog', 'fres', 'lexden'])
    avg_tuldava = float(df["tuldava"].mean())
    avg_fog = float(df["fog"].mean())
    avg_fres = float(df["fres"].mean())
    avg_lexden = float(df['lexden'].mean())
    return avg_tuldava, avg_fog, avg_fres, avg_lexden

print(calc_coeficient())