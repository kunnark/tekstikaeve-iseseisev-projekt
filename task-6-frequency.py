from collections import Counter
from helpers.read_data import read_file_as_list
from estnltk import Text

def content_word_lemmas(sentence, content_poses):
    text = Text(sentence)
    sent_lemmas = text.lemmas
    sent_postags = text.postags
    # content_poses = ['S', 'V', 'A', 'C', 'U']
    if content_poses is not None:
        lemma_list = [sent_lemmas[i].lower() for i in range(len(sent_lemmas)) if sent_postags[i] in content_poses]
    else:
        lemma_list = [sent_lemmas[i].lower() for i in range(len(sent_lemmas))]
    if len(lemma_list) > 0:
        return lemma_list
    else:
        return None

# Single words
fileread = read_file_as_list("../iseseisev_projekt/data/ppa.txt")
fileread = fileread.replace("ppa24h", "").replace("#", "")
lemmas = content_word_lemmas(fileread, ['S', 'A', 'C', 'U'])

counter = Counter(lemmas).most_common()
print(counter)

with open("iseseisev_projekt/results/task-6-similarities/frequencies-single.txt", "w") as target:
    target.write(" ".join([str(c[0] + " "+ str(c[1]) + "\n") for c in counter]))

# Bi-grams
lemmas = content_word_lemmas(fileread,
    [
        "C",
        "G",
        "H",
        "I",
        "K",
        "N",
        "O",
        "P",
        "S",
        "U",
        "V",
        "X",
        "Y"
    ]
)

i = 0
word_pairs = []
for i in range(0, len(lemmas)):
    if i < len(lemmas) - 1:
        word_pairs.append([lemmas[i], lemmas[i + 1]])

counter = Counter([str(w[0]+" "+w[1]) for w in word_pairs]).most_common()

with open("../iseseisev_projekt/results/task-6-similarities/frequencies-pairs.txt", "w") as target:
    target.write(" ".join([str(c[0] + " "+ str(c[1]) + "\n") for c in counter]))


