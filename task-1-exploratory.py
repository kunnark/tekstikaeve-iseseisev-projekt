from estnltk import Text
from collections import Counter

def read_file_as_list(file_path):
    with open(file_path, 'r') as src:
        return src.read()

def content_word_lemmas(sentence, content_poses):
    text = Text(sentence)
    sent_lemmas = text.lemmas
    sent_postags = text.postags
    # content_poses = ['S', 'V', 'A', 'C', 'U']
    if content_poses is not None:
        lemma_list = [sent_lemmas[i].lower() for i in range(len(sent_lemmas)) if sent_postags[i] in content_poses]
    else:
        lemma_list = [sent_lemmas[i].lower() for i in range(len(sent_lemmas))]
    if len(lemma_list) > 0:
        return lemma_list
    else:
        return None

def count_unique_words(file_path):
    text = read_file_as_list(file_path)
    lemmas = content_word_lemmas(text, None)
    col = Counter(lemmas).most_common()
    return len(col)

def count_all_words(file_path):
    text = read_file_as_list(file_path)
    return len(text.split(" "))

def count_documents(file_path):
    documents = read_file_as_list(file_path)
    count = documents.count("#ppa24h")
    return count

def pos_tags_list(file_path):
    text = read_file_as_list(file_path)
    text = Text(text)
    col = Counter(text.postags).most_common()
    for i in range(0, len(col)):
        print(map_postag_name(col[i][0]), col[i][1])

def map_postag_name(pos_tag):
    # https://estnltk.github.io/estnltk/1.4/tutorials/morf_tables.html
        types_map = {
            "A": "Omadussõna algvõrre",
            "C": "Omadussõna keskvõrre",
            "D": "Määrsõna",
            "G": "Genitiiv atribuut",
            "H": "Pärisnimi",
            "I": "Hüüdsõna",
            "J": "Sidesõna",
            "K": "Kaassõna",
            "N": "Põhiarvsõna",
            "O": "Järgarvsõna",
            "P": "Asesõna",
            "S": "Nimisõna",
            "U": "Omadussõna",
            "V": "Tegusõna",
            "X": "Sõna verbi juures - tähendus puudub",
            "Y": "Lühend",
            "Z": "Lausemärk"
        }
        try:
            if "|" in pos_tag:
                return types_map[list(pos_tag)[0]]  # simplifying analysis by removing parallel word types
            else:
                return types_map[pos_tag]
        except TypeError:
            pass
        return types_map[pos_tag]

########################################################################
# Execution
########################################################################
doc_count = count_documents("../iseseisev_projekt/data/ppa.txt")
all_words_count = count_all_words("../iseseisev_projekt/data/ppa.txt")
unique_words_count = count_unique_words("../iseseisev_projekt/data/ppa.txt")

print("Dokumente korpuses: ", doc_count)
print("Sõnu korpuses: ", all_words_count)
print("Unikaalseid sõnu korpuses: ", unique_words_count)
print("Keskmine dokumendi pikkus sõnades: ", str(all_words_count / doc_count))
print("Keskmiselt unikaalseid sõnu dokumendis: ", str(unique_words_count / doc_count))
print("Sõnatüübid")
print(pos_tags_list("../iseseisev_projekt/data/ppa.txt"))