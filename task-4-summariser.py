from iseseisev_projekt.helpers import summariser

def read_file_as_list(file_path):
    with open(file_path, 'r') as src:
        return src.read()

summary = summariser.generate_summary("../iseseisev_projekt/data/ppa.txt", 10)

with open("../iseseisev_projekt/results/task-4-summariser/summary.txt", 'w') as target:
    target.write(summary)
    target.close()