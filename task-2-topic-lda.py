import nadal_11.Topic_modeling_LDA as lda

NUM_TOPICS = 10
NUM_WORDS_IN_TOPICS = 20
stop_words = lda.stopwords_list("../nadal_11/source/stopwords_est.txt")
path = "../iseseisev_projekt/data/"
print("LDA")
lda.lda_main(path, stop_words, NUM_TOPICS, NUM_WORDS_IN_TOPICS, ['S'])