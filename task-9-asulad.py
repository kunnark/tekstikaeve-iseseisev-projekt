from iseseisev_projekt.helpers.read_data import read_file_as_list
from collections import Counter
from estnltk import Text

def content_word_lemmas(sentence, content_poses):
    text = Text(sentence)
    sent_lemmas = text.lemmas
    sent_postags = text.postags
    # content_poses = ['S', 'V', 'A', 'C', 'U']
    if content_poses is not None:
        lemma_list = [sent_lemmas[i].lower() for i in range(len(sent_lemmas)) if sent_postags[i] in content_poses]
    else:
        lemma_list = [sent_lemmas[i].lower() for i in range(len(sent_lemmas))]
    if len(lemma_list) > 0:
        return lemma_list
    else:
        return None

data = read_file_as_list("../iseseisev_projekt/data/ppa.txt")

lemmas = content_word_lemmas(data,
    [
        "H"
    ]
)

count_upper_words = Counter(lemmas).most_common()
# Hours
for item in count_upper_words:
    print(item[0], item[1])
