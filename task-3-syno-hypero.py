from estnltk import Text
from iseseisev_projekt.helpers.word_syno_hypero import is_hyperonym, is_synonym
import pandas as pd
import time

start_time = time.time()

def read_file_as_list(file_path):
    with open(file_path, 'r') as src:
        return src.read()

def content_word_lemmas(sentence, content_poses):
    text = Text(sentence)
    sent_lemmas = text.lemmas
    sent_postags = text.postags
    # content_poses = ['S', 'V', 'A', 'C', 'U']
    if content_poses is not None:
        lemma_list = [sent_lemmas[i].lower() for i in range(len(sent_lemmas)) if sent_postags[i] in content_poses]
    else:
        lemma_list = [sent_lemmas[i].lower() for i in range(len(sent_lemmas))]
    if len(lemma_list) > 0:
        return lemma_list
    else:
        return None

def word_pairs(file_path):
    data = read_file_as_list(file_path)
    text = Text(data)
    lemmas = text.lemmas
    pos_tags = text.postags

    target = []
    temp = []
    for i in range(0, len(pos_tags)):
        if 'S' in pos_tags[i] and len(temp) == 0:
            temp.append(lemmas[i])
        elif len(temp) > 0:
            if 'S' in pos_tags[i]:
                temp.append(lemmas[i])
                if "!" not in temp or "." not in temp or "?" not in temp:
                    if temp[0] != temp[len(temp) - 1]:
                        target.append([temp[0], temp[len(temp) - 1]])
                    temp = []
            else:
                temp.append(lemmas[i])
    return target


#######################################################################################
# Exec
#######################################################################################

word_pairs = word_pairs("../iseseisev_projekt/data/ppa.txt")

result = []
i = 0
print(len(word_pairs))
for pair in word_pairs:
    temp = [pair[0], pair[1], is_synonym(pair[0], pair[1]), is_hyperonym(pair[0], pair[1])]
    result.append(temp)
    i += 1
    if i % 50 == 0:
        print(i, str(time.time() - start_time))
df = pd.DataFrame(result, columns=['word_1', 'word_2', 'is_synonym', 'is_hyperonym'])
df.to_csv("../iseseisev_projekt/results/task-3-syno-hypero/task-3-syno-hypero.csv", index=False)

